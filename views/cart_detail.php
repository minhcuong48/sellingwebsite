<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cart | E-Shopper</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/prettyPhoto.css" rel="stylesheet">
    <link href="assets/css/price-range.css" rel="stylesheet">
    <link href="assets/css/animate.css" rel="stylesheet">
	<link href="assets/css/main.css" rel="stylesheet">
	<link href="assets/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<?php include("header.html");?>
	
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="index.php">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td>product_name</td>
							<td>price </td>
							<td>description </td>
							<td>company </td>
							<td>quantity </td>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($cart_entity as $cart_line) {
							echo '<tr>';
							echo '<td class="cart_description"><a href="product_detail?product_id=' . $cart_line->getId() . '"><p><h4>' . $cart_line->getName() . '</h4></p></a></td>';
							echo '<td class="cart_price"><p>' . $cart_line->getPrice() . ' VND</p></td>';
							echo '<td class="cart_description"><p>' . $cart_line->getDescription() . '</p></td>';
							echo '<td class="cart_description"><p>' . $cart_line->getCompany() . '</p></td>';
							echo '<td class="cart_quantity"><p>' . $cart_line->getQuantity() . '</p></td>';
							echo '</tr>';
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->

	<?php include("footer.html");?>
</body>
</html>