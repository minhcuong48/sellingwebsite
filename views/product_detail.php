<!-- version 1 -->
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Product Details | E-Shopper</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/prettyPhoto.css" rel="stylesheet">
    <link href="assets/css/price-range.css" rel="stylesheet">
    <link href="assets/css/animate.css" rel="stylesheet">
	<link href="assets/css/main.css" rel="stylesheet">
	<link href="assets/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
</head>
<body>
	<?php include("header.html");?>
	
<h1><?php
    if (isset($_SESSION['notify'])) {
        echo $_SESSION['notify'];
        unset($_SESSION['notify']);
    }
    ?></h1>
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-2"></div>
				
				<div class="col-sm-10 ">
					<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product">
								<img src="assets/images/product-details/1.jpg" alt="" />
							</div>

						</div>
						<div class="col-sm-7">
							<div class="product-information"><!--/product-information-->
								<img src="assets/images/product-details/new.jpg" class="newarrival" alt="" />
								<h2>Name: <?php echo $product->getName() ?></h2>
								<p>Description: <?php echo $product->getDescription() ?></p>
								<span>
									<span>Price: <?php echo $product->getPrice() ?> VND</span>
									<?php if (isset($_SESSION['idUser'])) { ?>
										<form action="add_to_cart" method="POST">
											<label>Quantity:</label>
											<input type="number" name="product_quantity">
											<input type="hidden" name="product_id" value="<?php echo $product->getId() ?>"><br>
											<input type="hidden" name="product_name" value="<?php echo $product->getName() ?>"><br>											
											<button type="submit" class="btn btn-fefault cart">
												<i class="fa fa-shopping-cart"></i>
												Add to cart
											</button>
										</form>
									<?php } ?>
								</span>
								<p><b>Company: <?php echo $product->getCompany() ?></b></p>
								
							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->
					
					
				</div>
			</div>
		</div>
	</section>
	
	<?php include("footer.html");?>
	
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>