<?php

require_once('models/order.php');

class ConfirmToPayController
{

    private static $instance;

    public function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new ConfirmToPayController();
        }
        return self::$instance;
    }

    private function __construct()
    {
    }

    function render()
    {
        $total_money = $_POST['total_money'];
        $order_id = $_POST['order_id'];
        $order = Order::find($order_id);
        // generate new bill from an order
        $bill = $order->generate_bill();
        if ($order->pay() && $bill->save_as_new()) {
            include_once('views/payment_successful.html');
        } else {
            $order->undo_pay();
            include_once('views/payment_failed.html');
        }
    }

}

?>