<?php

require_once('models/order.php');

//session_start();

class OrderDetailController
{
    private static $instance;

    public function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new OrderDetailController();
        }
        return self::$instance;
    }

    private function __construct()
    {
    }

    function render()
    {
        $order_id = $_POST['order_id'];
        //$username = $_SESSION['username'];
        $username = $order_id;
        $order = Order::find($order_id);
        if ($order != NULL) {
            require_once('views/order_detail.html');
        } else {
            //echo "order_id = $order_id";
        }
    }

}

//
//$order_detail_controller = new OrderDetailController();
//$order_detail_controller->render();

?>