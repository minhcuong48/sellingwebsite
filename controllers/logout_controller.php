<?php

class LogoutController
{
    private static $instance;

    public function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new LogoutController();
        }
        return self::$instance;
    }

    private function __construct()
    {
    }

    function render()
    {
        if (isset($_SESSION['username'])) {
            unset($_SESSION['username']); // xóa session login
            unset($_SESSION['idUser']); // xóa session login

            require_once('views/home_page.php');
        }
    }
}

?>
	