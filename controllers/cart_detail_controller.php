<?php
/**
 * Created by PhpStorm.
 * User: trieu_000
 * Date: 11/30/2017
 * Time: 1:45 PM
 */

class CartDetailController
{
    private static $instance;
    private $modelCart;

    public function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new CartDetailController();
        }
        self::$instance->user_id = $_SESSION['idUser'];
        self::$instance->modelCart = new ModelCart(self::$instance->user_id);
        return self::$instance;
    }

    private function __construct()
    {
    }

    private function get_cart()
    {
        $cart_id = $_SESSION['idUser'];// cart_id == user_id
        return self::$instance->modelCart->getCart($cart_id);
    }

    function render()
    {
        $cart_entity = $this->get_cart()->getEntityCartLine();
        include_once('views/cart_detail.php');
    }
}