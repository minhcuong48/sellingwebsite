<?php
/**
 * Created by PhpStorm.
 * User: trieu_000
 * Date: 11/22/2017
 * Time: 1:10 PM
 */

class HomePageController
{
    private static $instance;

    public function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new HomePageController();
        }
        return self::$instance;
    }

    private function __construct()
    {
    }

    public function render()
    {
        include_once('views/home_page.php');
    }

}