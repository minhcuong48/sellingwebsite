<?php

require_once('models/order.php');
session_start();

class PaymentController
{

    private static $instance;

    public function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new PaymentController();
        }
        return self::$instance;
    }

    private function __construct()
    {
    }

    function render()
    {
        if (isset($_SESSION['idUser'])) { // if user logged in
            $user_id = $_SESSION['idUser'];
            $username = $_SESSION['username'];
            $orders = Order::all_in_queue_of_user_id($user_id);
            include_once('views/payment.html');
        } else { // generate view for GMC
            $user_id = 4;
            $username = 'Giap Minh Cuong';
            $orders = Order::all_in_queue_of_user_id($user_id);
            include_once('views/payment.html');
        }
    }

}

?>

