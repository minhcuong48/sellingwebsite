<?php
/**
 * Created by PhpStorm.
 * User: trieu_000
 * Date: 11/23/2017
 * Time: 12:36 PM
 */

require_once('models/model_product.php');

class ProductDetailController
{
    private static $instance;

    public function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new ProductDetailController();
        }
        return self::$instance;
    }

    private function __construct()
    {
    }


    private function get_product()
    {
        $product_id = $_GET['product_id'];
        $model_product = new ModelProduct();
        return $model_product->getProduct($product_id);
    }

    function render()
    {
        $product = $this->get_product();
        include_once('views/product_detail.php');
    }
}
