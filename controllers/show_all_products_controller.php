<?php

class ShowallproductController
{
    private static $instance;

    public function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new ShowallproductController();
        }
        return self::$instance;
    }

    private function __construct()
    {
    }

    public function render()
    {
        require_once("models/model_product.php");
        $proModel = new ModelProduct();
        $product_result = $proModel->showallproduct();
        include_once("views/search_view.html");
    }
}

?>
