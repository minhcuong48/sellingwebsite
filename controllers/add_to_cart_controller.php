<?php
/**
 * Created by PhpStorm.
 * User: trieu_000
 * Date: 11/18/2017
 * Time: 9:11 AM
 */
require_once('models/model_cart.php');
require_once('models/model_product.php');
require_once('models/entity_product.php');

class AddToCartController
{
    private $user_id;
    private $modelCart;
    private $modelProduct;

    private static $instance;

    public function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new AddToCartController();
        }
        self::$instance->user_id = $_SESSION['idUser'];
        self::$instance->modelCart = new ModelCart(self::$instance->user_id);
        return self::$instance;
    }

    private function __construct()
    {
    }

    function addToCart($product)
    {
        if ($product->getQuantity() <= 0) {
            $_SESSION['notify'] = "Error: Quantity <= 0";
        } else if (self::$instance->isEnoughProduct($product)) {
            $result = self::$instance->modelCart->add($product);
            if ($result == false) {
                $_SESSION['notify'] = "Error when excute SQL";
            } else {
                $_SESSION['notify'] = "Add to cart success";
            }
        } else {
            $_SESSION['notify'] = "Don't enough product in warehourse";
        }
        include_once('views/product_detail.php');
    }

    private function isEnoughProduct($product)
    {
        self::$instance->modelProduct = new ModelProduct();
        if ($productInWareHourse = self::$instance->modelProduct->getProduct($product->getId())) {
            if ($product->getQuantity() <= $productInWareHourse->getQuantity()) {
                return true;
            }
        }
        return false;
    }
}
