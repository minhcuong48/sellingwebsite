<?php
include_once 'models/user_model.php';

class SignUpController
{

    private static $instance;

    public function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new SignUpController();
        }
        return self::$instance;
    }

    private function __construct()
    {
    }

    function render()
    {
        $check = $this->sendDataToUser();
        if ($check) {
            require_once('views/home_page.html');
        }
    }

    function sendDataToUser()
    {
        $data["name"] = isset($_POST["name"]) ? $_POST["name"] : '';
        $data["email"] = isset($_POST["email"]) ? $_POST["email"] : '';
        $data["password"] = isset($_POST["pass"]) ? $_POST["pass"] : '';
        $data["confirm_password"] = isset($_POST["confirm_pass"]) ? $_POST["confirm_pass"] : '';
        if ($data["name"] == '' && $data["email"] == '' && $data["password"] == '' && $data["confirm_password"] == '') {
            require_once('views/signup_form.html');
        } else {
            $user = new User($data["name"], $data["email"], $data["password"]);
            return ($user->insertToDatabase($data["confirm_password"]));
        }

    }
}

?>