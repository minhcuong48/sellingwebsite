<?php

class SearchController
{
    private static $instance;

    public function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new SearchController();
        }
        return self::$instance;
    }

    private function __construct()
    {
    }

    public function render()
    {
        $search = $_GET["search"];
        require_once("models/model_product.php");

        $proModel = new ModelProduct();
        $product_result = $proModel->searchProduct($search);

        include_once("views/search_view.html");
    }

}

?>
