<?php
//require_once('../controllers/MainController.php');
require_once('models/user_model.php');

class LoginController
{
    private static $instance;

    public function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new LoginController();
        }
        return self::$instance;
    }

    private function __construct()
    {
    }

    public function renderView()
    {
        //include_once('views/login_form.html');
        if (isset($_POST['email'])) {
            $e = $_POST['email'];
            echo "email = $e";
            $this->sendDataToUser1();
        }
    }

    function render()
    {
        $check = $this->sendDataToUser1();
        //echo $check;
        if ($check) {
            require_once('views/user.php');
        }
    }

    function render_user_page()
    {
        require_once('views/user.php');
    }

    public function sendDataToUser1()
    {
        $data["email"] = isset($_POST["email"]) ? $_POST["email"] : '';
        $data["password"] = isset($_POST["pass"]) ? $_POST["pass"] : '';
        if ($data["email"] == '' && $data["password"] == '') {
            require_once('views/login_form.html');
        } else {
            $user = new User("", $data["email"], $data["password"]);
            return ($user->checkLogin());
        }
    }
}

?>