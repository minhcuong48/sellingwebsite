<?php
/*
* GUIDE
* CREATE DATABASE
* 	$db = new Database(servername, username, password); //if you dont pass any params then that method will use default params as below.
* 	$db->create_database(database_name);
* $db will auto close connect.
* QUERY insert, update, delete
* 	$db = new Database();
*	$db->connect_database(exist_database_name);
* 	$db->query(sql);
* QUERY select
* 	$db = new Database();
* 	$db->connect_database(exist_database_name);
* 	$result = $db->query(sql);
*	if ($result->num_rows > 0) {
*		while ($row = $result->fetch_assoc()) {
*			$attribute = $row['attribute']
*			...	
*		}		
*	}
* NOTE: always connect_database() before use query() except creating new database.
*/

class Database {

	protected $conn;
	protected $dbname;

	function __construct($servername = 'localhost', $username = 'root', $password = '') {
		$this->servername = $servername;
		$this->username = $username;
		$this->password = $password;
		$this->dbname = NULL;
	}

	function __destruct() {
		$this->conn->close();
	}

	// private function

	private function get_connect($servername, $username, $password) {
		// Create connection
		$this->conn = new mysqli($servername, $username, $password);

		// Check connection
		if ($this->conn->connect_error) {
		    $this->conn = NULL;
		}
	}

	private function is_connected_to_database() {
		return $this->dbname != NULL ? true : false;
	}

	// public function

	function create_database($db_name) {
		$this->get_connect($this->servername, $this->username, $this->password);
		if ($this->conn == NULL) {
			return false;
		}
		$sql = "CREATE DATABASE IF NOT EXISTS $db_name";
		if(mysqli_query($this->conn, $sql)) {
			return true;
		}
		
		return false;
	}

	function connect_database($db_name) {
		$this->conn = new mysqli($this->servername, $this->username, $this->password, $db_name);
		if ($this->conn->connect_error) {
			$this->dbname = NULL;
			$this->conn = NULL;
			return false;
		}
		$this->dbname = $db_name;
		return true;
	}

	function query($sql) {
		return $this->is_connected_to_database() ? $this->conn->query($sql) : false;
	}
	
}

?>