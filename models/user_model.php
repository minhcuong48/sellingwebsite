<?php
	require_once('helpers/database.php');
	//require_once('controllers/login_controller.php');
	class User{
		private $name;
		private $email;
		private $password;

		public function __construct($name, $email, $password){
			$this->name = $name;
			$this->email = $email;
			$this->password = $password;
		}

		public function insertToDatabase($confirm_password){
			if($this->validate_user_for_signup($confirm_password)){
				$db = $this->connectDB();
				$kt = $db->query("SELECT * from user where email = '$this->email'");
				if(mysqli_num_rows($kt) > 0){
					echo "Email bi trung. Khong the insert.";
					return 0;
				}
				else{
					$kt = $db->query("INSERT INTO user(name,email, password) VALUES ('$this->name', '$this->email', '$this->password')");
					if($kt){
						echo "Insert success!";
						//header("Location: ../views/home_page.html");
						//$home_page_controller = new homepagecontroller();
						//$home_page_controller->renderView();
						return 1;
						exit;
					}
					else{
						echo "Insert fail!";
					}
				}
			}
			else{
				echo "Du lieu khong hop le. Khong the insert.";
			}
		}

		public function checkLogin(){
			if($this->validate_user_for_login()){
				$db = $this->connectDB();
				$result = $db->query("SELECT * from user where email = '$this->email' and password = '$this->password'");
				if(mysqli_num_rows($result) > 0){
					echo "Log in success! <br>";
					while ($row = $result->fetch_assoc()) {
						$this->name = $row['Name'];
						//session_start();
						$_SESSION['username'] = $this->name;
						$_SESSION['email'] = $this->email;
						$_SESSION['idUser'] = $row['ID']; 
						//$_SESSION['password'] = $this->password;
						echo "Chuc mung ".$_SESSION['username']." da dang nhap thanh cong!";
						//header("Location: ../views/user.php");
						//$user_controller = new UserController();
						//$user_controller->renderView();
						return 1;
						exit;

					}		
				}
				else{
					echo "Email or password is not valid!";
					return 0;
				}
			}
		}

		private function validate_user_for_signup($confirm_password){
			$error = array();
			if(empty($this->name)){
				$error["name"] = "Ban chua nhap ten";
				echo $error["name"] ."<br>";
			}
			if(empty($this->email)){
				$error["email"] = "Ban chua nhap email";
				echo $error["email"]."<br>";
			}
			else if(!$this->is_email($this->email)){
				$error["email"] = "Email chua dung dinh dang";
				echo $error["email"]."<br>";
			}

			if (empty($this->password)) {
				$error["password"] = "Ban chua nhap pass";
				echo $error["password"]."<br>";
			}
			else if(strlen($this->password) < 6){
				$error["password"] = "Do dai password toi thieu la 6 ki tu!";
				echo $error["password"]."<br>";
			}
			else if($this->password != $confirm_password){
				$error["password"] = "Password repeat is not correct! ";
				echo $error["password"]."<br>";
			}

			if(!$error){
				echo "Du lieu co the luu tru.";
				return 1;
			}
			else{
				echo "Du lieu bi loi.<br>";
				return 0;
			}
		}

		private function validate_user_for_login(){
			$error = array();
			if(empty($this->email)){
				$error["email"] = "Ban chua nhap email";
				echo $error["email"]."<br>";
			}
			else if(!$this->is_email($this->email)){
				$error["email"] = "Email chua dung dinh dang";
				echo $error["email"]."<br>";
			}
			if (empty($this->password)) {
				$error["password"] = "Ban chua nhap pass";
				echo $error["password"]."<br>";
			}
			if(!$error){
				return 1;
			}
			else{
				return 0;
			}
		}

		private function is_email($str){
			return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
		}

		private function connectDB(){
			$db = new Database("localhost", "root", "");
			$db->connect_database("sellingwebsite");
			return $db;
		}
	}

?>