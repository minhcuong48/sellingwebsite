<?php
/**
 * Created by PhpStorm.
 * User: trieu_000
 * Date: 11/30/2017
 * Time: 1:59 PM
 */

class EntityCartLine
{
    private $id;
    private $name;
    private $price;
    private $description;
    private $company;
    private $quantity;

    /**
     * EntityCartLine constructor.
     * @param $name
     * @param $price
     * @param $description
     * @param $company
     * @param $quantity
     */
    public function __construct($id, $name, $price, $description, $company, $quantity)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->description = $description;
        $this->company = $company;
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }


}