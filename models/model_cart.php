<?php
/**
 * Created by PhpStorm.
 * User: trieu_000
 * Date: 11/18/2017
 * Time: 9:25 AM
 */

require_once('helpers/database.php');
require_once('models/entity_cart.php');
require_once('models/entity_cart_line.php');

class ModelCart
{
    private $db;
    private $user_id;

    public function __construct($user_id)
    {
        $this->user_id = $user_id;
        $this->db = new Database();
        $this->db->connect_database('sellingwebsite');
    }

    function add($product)
    {
        $sql = "INSERT INTO cart (id, product_id, quantity) 
                        VALUES ($this->user_id, " . $product->getId() . ", " . $product->getQuantity() . ")
                          ON DUPLICATE KEY UPDATE quantity = " . $product->getQuantity();
        return $this->db->query($sql);
    }

    /*
     * cart_id == user_id
     */
    function getCart($cart_id)
    {
        $entity_cart = new EntityCart();
        $sql = "SELECT products.id, products.product_name, products.price, products.description, products.company, cart.quantity 
                FROM `cart` JOIN products WHERE cart . id = $cart_id AND cart . product_id = products . id";
        $result = $this->db->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $id = $row['id'];
                $name = $row['product_name'];
                $price = $row['price'];
                $description = $row['description'];
                $company = $row['company'];
                $quantity = $row['quantity'];
                $entity_cart->push(new EntityCartLine($id, $name, $price, $description, $company, $quantity));
            }
        }
        return $entity_cart;
    }
}