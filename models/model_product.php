<?php
/**
 * Created by PhpStorm.
 * User: trieu_000
 * Date: 11/18/2017
 * Time: 9:19 AM
 */

require_once('helpers/database.php');
require_once('models/entity_product.php');

class ModelProduct
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
        $this->db->connect_database('sellingwebsite');
    }

    function getProduct($product_id)
    {
        $sql = "SELECT * FROM products WHERE id = $product_id";
        $result = $this->db->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $id = $row['id'];
                $name = $row['product_name'];
                $quantity = $row['quantity'];
                $price = $row['price'];
                $description = $row['description'];
                $company = $row['company'];
                $product = new EntityProduct($id, $name, $quantity);
                $product->setPrice($price);
                $product->setDescription($description);
                $product->setCompany($company);
                return $product;
            }
        }
        return;
    }

    public function searchProduct($search)
    {
        $db = new Database();
        $db->connect_database('sellingwebsite');
        $query_search = "SELECT * FROM products WHERE id LIKE '%" . $search . "%'or product_name LIKE '%" . $search . "%' or company LIKE '%" . $search . "%'";
        $result = $db->query($query_search);

        $product_result = array();

        if ($result && $result->num_rows > 0) {
            while ($pros = mysqli_fetch_assoc($result)) {
                $product_result[] = $pros;
            }
        }
        return $product_result;
    }

    public function showallproduct()
    {
        $db = new Database();
        $db->connect_database('sellingwebsite');
        $query_search = "SELECT * FROM products";
        $result = $db->query($query_search);

        $product_result = array();

        if ($result->num_rows > 0) {
            while ($pros = mysqli_fetch_assoc($result)) {
                $product_result[] = $pros;
            }
        }

        return $product_result;
    }
}
