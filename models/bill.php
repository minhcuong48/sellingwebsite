<?php

require_once('helpers/database.php');

class Bill {
	private $id;
	private $pay_date;
	private $order_id;

	function __construct($id, $pay_date, $order_id) {
		$this->id = $id;
		$this->pay_date = $pay_date;
		$this->order_id = $order_id;
	}

	function get_pay_date_time_string() {
		return date_format($this->pay_date, 'Y-m-d H:i:s');
	}

	function save_as_new() {
		$db = new Database();
		$db->connect_database('sellingwebsite');
		$pay_date = $this->get_pay_date_time_string();
		$sql = "INSERT INTO bills(id, pay_date, order_id) VALUES ('$this->id', '$pay_date', '$this->order_id');";
		return $db->query($sql);
	}

	// static method
	static function new_bill($order_id) {
		// get maxid
		$new_id = 0;
		$db = new Database();
		$db->connect_database('sellingwebsite');
		$sql = "SELECT max(id) as max FROM bills;";
		$result = $db->query($sql);
		if($result->num_rows > 0 && $row = $result->fetch_assoc()) {
			$new_id = $row['max'];
		}
		$new_id += 1;
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		return new Bill($new_id, new DateTime(), $order_id);
	}
}

?>