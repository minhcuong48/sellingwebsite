<?php

require_once('helpers/database.php');

class OrderLine {
	private $id;
	private $product_id;
	private $quantity;
	private $order_id;

	function __construct($id, $product_id, $quantity, $order_id) {
		$this->id = $id;
		$this->product_id = $product_id;
		$this->quantity = $quantity;
		$this->order_id = $order_id;
	}

	// getter and setter
	function get_id() {
		return $this->id;
	}

	function get_product_id() {
		return $this->product_id;
	}

	function get_quantity() {
		return $this->quantity;
	}

	function get_order_id() {
		return $this->order_id;
	}

	// public
	function get_product_name() {
		$db = new Database();
		$db->connect_database('sellingwebsite');
		$sql = "SELECT product_name FROM products WHERE id = $this->product_id;";
		$result = $db->query($sql);
		if($result->num_rows > 0 && $row = $result->fetch_assoc()) {
			return $row['product_name'];
		} else {
			return "";
		}
	}

	function get_product_price() {
		$db = new Database();
		$db->connect_database('sellingwebsite');
		$sql = "SELECT price FROM products WHERE id = $this->product_id;";
		$result = $db->query($sql);
		if($result->num_rows > 0 && $row = $result->fetch_assoc()) {
			return $row['price'];
		} else {
			return "";
		}
	}

	// get order lines of a order
	static function get_all_lines_of_order_id($order_id) {
		$array = array();
		$db = new Database();
		$db->connect_database('sellingwebsite');

		$sql = "SELECT * FROM orderlines WHERE order_id = $order_id;";
		$result = $db->query($sql);
		if($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$new_order_line = OrderLine::get_line_from_result_row($row);
				array_push($array, $new_order_line);
			}
		}
		return $array;
	}

	// private
	private static function get_line_from_result_row($row) {
		$id = $row['id'];
		$product_id = $row['product_id'];
		$quantity = $row['quantity'];
		$order_id = $row['order_id'];

		return new OrderLine($id, $product_id, $quantity, $order_id);
	}
}

?>