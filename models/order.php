<?php

require_once('helpers/database.php');
require_once('models/order_line.php');
require_once('models/bill.php');

class Order {

	private $id;
	private $date;
	private $paid;
	private $user_id;

	function __construct($id, $date, $paid, $user_id) {
		$this->id = $id;
		$this->date = $date;
		$this->paid = $paid;
		$this->user_id = $user_id;
	}

	// getter and setter
	function get_order_date_string() {
		return date_format($this->date, 'd/m/Y');
	}

	function get_id() {
		return $this->id;
	}

	// get all order lines of this order
	function get_order_lines() {
		return OrderLine::get_all_lines_of_order_id($this->id);
	}

	function pay() {
		$db = new Database();
		$db->connect_database('sellingwebsite');
		$sql = "UPDATE orders SET paid = 1 WHERE id = $this->id;";
		return $db->query($sql);
	}

	function undo_pay() {
		$db = new Database();
		$db->connect_database('sellingwebsite');
		$sql = "UPDATE orders SET paid = 0 WHERE id = $this->id;";
		return $db->query($sql);
	}

	function generate_bill() {
		return Bill::new_bill($this->id);
	}

	// static method
	// get all orders which are waitting to pay.
	static function all_in_queue() {
		$orders = array();

		$db = new Database();
		$db->connect_database('sellingwebsite');

		$sql = "SELECT * FROM orders WHERE paid = 0;";
		$result = $db->query($sql);
		if($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$id = $row['id'];
				$date = date_create($row['order_date']);
				$paid = $row['paid'];
				$user_id = $row['user_id'];

				$new_order = new Order($id, $date, $paid, $user_id);
				array_push($orders, $new_order);
			}
		}

		return $orders;
	}

	// get all orders which are waitting to pay of a user.
	static function all_in_queue_of_user_id($user_id) {
		$orders = array();

		$db = new Database();
		$db->connect_database('sellingwebsite');

		$sql = "SELECT * FROM orders WHERE paid = 0 AND user_id = $user_id;";
		$result = $db->query($sql);
		if($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$id = $row['id'];
				$date = date_create($row['order_date']);
				$paid = $row['paid'];
				$user_id = $row['user_id'];

				$new_order = new Order($id, $date, $paid, $user_id);
				array_push($orders, $new_order);
			}
		}

		return $orders;
	}

	// find order by id
	static function find($order_id) {
		$db = new Database();
		$db->connect_database('sellingwebsite');

		$sql = "SELECT * FROM orders WHERE id = $order_id;";
		$result = $db->query($sql);
		if ($result->num_rows > 0 && $row = $result->fetch_assoc()) {
			return Order::get_order_from_result_row($row);
		}
		return NULL;
	}



	// util method
	private function get_order_from_result_row($row) {
		$id = $row['id'];
		$date = date_create($row['order_date']);
		$paid = $row['paid'];
		$user_id = $row['user_id'];

		return new Order($id, $date, $paid, $user_id);
	}
}

?>