<?php
/**
 * Created by PhpStorm.
 * User: trieu_000
 * Date: 11/30/2017
 * Time: 1:58 PM
 */
require_once('models/entity_cart_line.php');

class EntityCart
{
    private $entity_cart_line = array();

    public function push($line)
    {
        array_push($this->entity_cart_line, $line);
    }

    /**
     * @return array
     */
    public function getEntityCartLine()
    {
        return $this->entity_cart_line;
    }
}