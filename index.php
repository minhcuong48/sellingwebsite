<?php

require_once('route.php');

require_once('controllers/payment_controller.php');
require_once('controllers/order_detail_controller.php');
require_once('controllers/confirm_to_pay_controller.php');

require_once('controllers/login_controller.php');
require_once('controllers/logout_controller.php');
require_once('controllers/signup_controller.php');
require_once('controllers/home_page_controller.php');

require_once('controllers/product_detail_controller.php');
require_once('controllers/add_to_cart_controller.php');
require_once('controllers/show_all_products_controller.php');
require_once('controllers/search_controller.php');
require_once('controllers/cart_detail_controller.php');

Route::set('payment', function () {
    $controller = PaymentController::getInstance();
    $controller->render();
});

Route::set('order_detail', function () {
    $controller = OrderDetailController::getInstance();
    $controller->render();
});

Route::set('confirm_to_pay', function () {// khong chay tu day
    $controller = ConfirmToPayController::getInstance();
    $controller->render();
});

Route::set('log_in', function () {
    $controller = LoginController::getInstance();
    $controller->render();
});

Route::set('log_out', function () {
    $controller = LogoutController::getInstance();
    $controller->render();
});

Route::set('sign_up', function () {
    $controller = SignUpController::getInstance();
    $controller->render();
});

Route::set('back_user', function () {
    $controller = LoginController::getInstance();
    $controller->render_user_page();
});

if (!isset($_GET['url'])) {
    $controller = HomePageController::getInstance();
    $controller->render();
}

Route::set('product_detail', function () {
    $controller = ProductDetailController::getInstance();
    $controller->render();
});

Route::set('add_to_cart', function () {// khong chay tu day
    $controller = AddToCartController::getInstance();
    $controller->addToCart(new EntityProduct($_POST['product_id'], $_POST['product_name'], $_POST['product_quantity']));
});

Route::set('showallproducts', function () {
    $controller = ShowallproductController::getInstance();
    $controller->render();
});

Route::set('search', function () {// khong chay tu day
    $controller = SearchController::getInstance();
    $controller->render();
});

Route::set('cart_detail', function () {
    $controller = CartDetailController::getInstance();
    $controller->render();
});

?>